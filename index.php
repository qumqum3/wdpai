<?php

require 'Router.php';

$path = trim($_SERVER['REQUEST_URI'], '/');
$path = parse_url( $path, PHP_URL_PATH);

Router::get('', 'DefaultController');
Router::get('videos', 'VideoController');
Router::post('login', 'SecurityController');
Router::post('addVideo', 'VideoController');
Router::post('register', 'SecurityController');
Router::post('search', 'VideoController');
Router::post('profile', 'UserController');
Router::post('settings', 'SecurityController');
Router::post('change', 'UserController');
Router::post('upgrade', 'UserController');
Router::get('like', 'VideoController');
Router::get('dislike', 'VideoController');


Router::run($path);


