<?php

require_once 'Repository.php';
require_once __DIR__.'/../models/Video.php';
require_once __DIR__ . '/../repository/UserRepository.php';

class VideoRepository extends Repository
{
    private $userRepository;

    public function getVideo(int $id): ?Video
    {
        $stmt = $this->database->connect()->prepare('
            SELECT * FROM videos WHERE id = :id
        ');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        $video = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($video == false) {
            return null;
        }

        return new Video(
            $video['title'],
            $video['description'],
            $video['video']
        );
    }

    public function addVideo(Video $video): void
    {
        $this->userRepository = new UserRepository();

        $date = new DateTime();
        $stmt = $this->database->connect()->prepare('
            INSERT INTO videos (title, description, created_at, id_assigned_by, video_name)
        values(?, ?, ?, ?, ?)'
        );
        $email = $_COOKIE['user'];
        $assignedById = $this->userRepository->convertEmailOnId($email);
        $stmt->execute([
        $video->getTitle(),
        $video->getDescription(),
        $date->format('Y-m-d'),
        $assignedById,
        $video->getVideo()
    ]);
    }

    public function getVideos(): array
    {
        $result = [];
        $stmt = $this->database->connect()->prepare(
            'SELECT * FROM videos'
        );
        $stmt->execute();
        $videos = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($videos as $video)
        {
            $video_object = new Video($video['title'], $video['description'],$video['video'],$video['like'],$video['dislike'],$video['id']);
            $video_object->setImage($video['video_name']);
            $result[] = $video_object;
        }

        return $result;
    }

    public function getVideoByTitle(string $searchString)
    {
        $searchString = '%'.strtolower($searchString).'%';

        $stmt = $this->database->connect()->prepare(
            'SELECT * FROM videos WHERE LOWER(title) LIKE :search OR LOWER(description) LIKE :search
        ');
        $stmt->bindParam(':search', $searchString, PDO::PARAM_STR);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function like(int $id)
    {
     $stmt = $this->database->connect()->prepare
     ('
        UPDATE videos SET "like" = "like" + 1 WHERE id = :id
     ');
     $stmt->bindParam(':id', $id, PDO::PARAM_INT);
     $stmt->execute();
    }

    public function dislike(int $id)
    {
        $stmt = $this->database->connect()->prepare
        ('
        UPDATE videos SET "dislike" = "dislike" + 1 WHERE id = :id
     ');
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
    }
}