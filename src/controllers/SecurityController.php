<?php

require_once 'AppController.php';
require_once __DIR__ .'/../models/User.php';
require_once __DIR__.'/../repository/UserRepository.php';

class SecurityController extends AppController {

    private $userRepository;

    public function __construct()
    {
        parent::__construct();
        $this->userRepository = new UserRepository();
    }

    public function login()
    {
        if (!$this->isPost())
        {
            return $this->render('login');
        }

        if (isset($_POST['register_button']))
        {
            return $this->render('register');
        }
        else if (isset($_POST['login_button']))
        {
            $email = $_POST['email'];
            $password = md5(md5($_POST['password']));

            $user = $this->userRepository->getUser($email);

            if (!$user) {
                return $this->render('login', ['messages' => ['User not found!']]);
            }

            if ($user->getEmail() !== $email) {
                return $this->render('login', ['messages' => ['User with this email not exist!']]);
            }

            if ($user->getPassword() !== $password) {
                return $this->render('login', ['messages' => ['Wrong password!']]);
            }

            setcookie('user',$_POST['email'], time()+ 3600);
            $url = "http://$_SERVER[HTTP_HOST]";
            header("Location: {$url}/videos");
        }
    }

    public function register()
    {
        if (!$this->isPost())
        {
            return $this->render('register');
        }

        $email = $_POST['email'];
        $password = $_POST['password'];
        $confirmedPassword = $_POST['confirmedPassword'];
        $name = ucfirst(strtolower($_POST['name']));
        $surname = ucfirst(strtolower($_POST['surname']));
        $phone = $_POST['phone'];

        if($email == null or
            $password == null or
            $confirmedPassword == null or
            $name == null or
            $surname == null) // phone not required
        {
            return $this->render('register', ['messages' => ['Fill required fields!']]);
        }

        if ($password !== $confirmedPassword) {
            return $this->render('register', ['messages' => ['Please provide proper password']]);
        }
        $user = new User($email, md5(md5($password)), $name, $surname);
        $user->setPhone($phone);

        $this->userRepository->addUser($user);

        return $this->render('login', ['messages' => ['You\'ve been succesfully registrated!']]);
    }

    public function settings()
    {
        if(isset($_COOKIE['user']) != null)
        {
            $email = $_COOKIE['user'];
            $userType = $this->userRepository->getUserType($email);
            $this->render( 'settings', ['userType'=> $userType]);
        }
        else
        {
            $this->render( 'login');
        }
    }
}