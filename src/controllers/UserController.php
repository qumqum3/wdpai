<?php
require_once 'AppController.php';
require_once __DIR__ . '/../models/User.php';
require_once __DIR__ . '/../repository/UserRepository.php';

class UserController extends AppController
{
    const MAX_FILE_SIZE = 1024*1024;
    const SUPPORTED_TYPES = ['image/png', 'image/jpeg'];
    const UPLOAD_DIRECTORY = '/../public/profiles/';

    private $userRepository;

    public function __construct()
    {
        parent::__construct();
        $this->userRepository = new UserRepository();
    }

    public function profile()
    {
        if(isset($_COOKIE['user']) != null)
        {
            $user = $this->userRepository->getUserByEmail($_COOKIE['user']);
            $this->render('profile', ['user' => $user]);
        }
        else
        {
                $this->render('login');
        }

    }

    public function change()
    {
        $name = $_POST['name'];
        $surname = $_POST['surname'];
        $phone = $_POST['phone'];
        $email = $_COOKIE['user'];
        $id = $this->userRepository->convertEmailOnId($email);
        $user = $this->userRepository->getUserByEmail($_COOKIE['user']);
        if($name != null)
        {
            $name = ucfirst(strtolower($name));
            $this->userRepository->changeName($name, $id);
        }

        if($surname != null)
        {
            $surname = ucfirst(strtolower($surname));
            $this->userRepository->changeSurname($surname, $id);
        }

        if($phone != null)
        {
            $phone = ucfirst(strtolower($phone));
            $this->userRepository->changePhone($phone, $id);
        }

        $this->render( 'profile', ['user' => $user]);
    }

    public function upgrade()
    {
        $email = $_COOKIE['user'];
        $user = $this->userRepository->upgradeToPhysio($email);
        $userType = $this->userRepository->getUserType($email);
        $this->render( 'settings', ['user' => $user, 'userType' => $userType]);
    }

}