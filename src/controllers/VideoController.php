<?php

require_once 'AppController.php';
require_once __DIR__.'/../models/Video.php';
require_once __DIR__.'/../repository/VideoRepository.php';
require_once __DIR__.'/../repository/UserRepository.php';

class VideoController extends AppController
{
    const MAX_FILE_SIZE = 1024*1024;
    const SUPPORTED_TYPES = ['image/png', 'image/jpeg'];
    const UPLOAD_DIRECTORY = '/../public/uploads/';

    private $messages = [];
    private $videoRepository;
    private $userRepository;


    public function __construct()
    {
        parent::__construct();
        $this->videoRepository = new VideoRepository();
        $this->userRepository = new UserRepository();
    }

    public function addVideo()
    {
        if ($this->isPost() && is_uploaded_file($_FILES['file']['tmp_name']) && $this->validate($_FILES['file'])) {
            move_uploaded_file($_FILES['file']['tmp_name'],
                dirname(__DIR__).self::UPLOAD_DIRECTORY.$_FILES['file']['name']
            );

            $video = new Video($_POST['title'], $_POST['description'], $_FILES['file']['name']);
            $this->videoRepository->addVideo($video);

            return $this->render('videos',
                ['videos' => $this->videoRepository->getVideos(),
            'messages' => $this->messages]);
        }
        return $this->render('add-video', ['messages' => $this->messages]);
    }

    public function videos(){
        $email = $_COOKIE['user'];
        $videos = $this->videoRepository->getVideos();
        if($email != null)
        {
            $userType = $this->userRepository->getUserType($email);
            $this->render('videos',['videos' => $videos, 'userType' => $userType]);
        }
        else
        {
            $this->render('videos',['videos' => $videos]);
        }
    }

    public function search()
    {
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
        if($contentType === "application/json")
        {
            $content = trim(file_get_contents("php://input"));
            $decoded = json_decode($content, true);

            header('Content-type: application/json');
            http_response_code(200);

            echo json_encode($this->videoRepository->getVideoByTitle($decoded['search']));
        }

    }

    public function like(int $id)
    {
        $this->videoRepository->like($id);
        http_response_code(200);
    }

    public function dislike(int $id)
    {
        $this->videoRepository->dislike($id);
        http_response_code(200);
    }

    private function validate(array $file): bool
    {
        if ($file['size'] > self::MAX_FILE_SIZE) {
            $this->messages[] = 'File is too large for destination file system.';
            return false;
        }

        if (!isset($file['type']) || !in_array($file['type'], self::SUPPORTED_TYPES)) {
            $this->messages[] = 'File type is not supported.';
            return false;
        }
        return true;
    }


}