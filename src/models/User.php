<?php


class User
{
    private $email;
    private $password;
    private $name;
    private $surname;
    private $phone;
    private $photo;
    private $userType;

    public function __construct(string $email,string $password,string $name,
                                string $surname, string $phone = "",
                                string $photo = "default.png",
                                string $userType = "viewer")
    {
        $this->email = $email;
        $this->password = $password;
        $this->name = $name;
        $this->surname = $surname;
        $this->phone = $phone;
        $this->photo = $photo;
        $this->userType = $userType;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password)
    {
        $this->password = $password;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name)
    {
        $this->name = $name;
    }

    public function getSurname(): string
    {
        return $this->surname;
    }

    public function setSurname(string $surname)
    {
        $this->surname = $surname;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone($phone): void
    {
        $this->phone = $phone;
    }

    public function getPhoto() : ?string
    {
        return $this->photo;
    }

    public function setPhoto(?string $photo)
    {
        $this->photo = $photo;
    }

    public function getUserType() : ?string
    {
        return $this->userType;
    }

    public function setUserType(?string $userType)
    {
        $this->photo = $userType;
    }
}