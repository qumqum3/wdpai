<!DOCTYPE html>

<head>
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <title>LOGIN PAGE</title>
</head>
<body>
    <div class="containter">
        <div class="logo">
            <img src="public/img/logo.svg">
        </div>
        <div class="login-containter">
            <form class="login" action="login" method="POST">
                <div class="message">
                    <?php
                        if(isset($messages)){
                             foreach($messages as $message){
                                 echo $message;
                             }
                         }
                         ?>
                </div>
                <input name="email" type="text" placeholder="email@email.com">
                <input name="password" type="password" placeholder="password">
                <div class="buttons">
                    <button type="submit" name="login_button" value="login">Login</button>
                    <button type="submit" name="register_button" value="register">Register</button>
                </div>
            </form>
        </div>
    </div>
</body>