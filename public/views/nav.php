<?php
if(isset($_POST['logout']))
{
    setcookie('user','',time()-1000);
    $url = "http://$_SERVER[HTTP_HOST]";
    header("Location: {$url}/videos");
}
?>


<div id="my-nav" class="sidenav open-nav">

    <a href="javascript:void(0)" class="closebtn" id="closebtn" onclick="closeNav()">&times;</a>
    <ul>
        <li>
            <a href="videos" ><img src="public/img/logo.svg" class="logo"></a>
        </li>

        <li>
            <a href="profile" class="button" action="profile" method="POST">
                <i class="fas fa-user"></i>
                Profile
            </a>
        </li>
        <li>
            <a href="#" class="button">
                <i class="fas fa-user-md"></i>
                Physio
            </a>
        </li>
        <li>
            <a href="#" class="button">
                <i class="fas fa-envelope"></i>
                Messages
            </a>
        </li>
        <li>
            <a href="#" class="button">
                <i class="fas fa-flag"></i>
                Alerts
            </a>
        </li>

        <li>
            <a href="settings" class="button" action="settings" method="POST">
                <i class="fas fa-cogs"></i>
                Settings
            </a>
        </li>

        <li id="nav_logout">
            <?php
            if(!isset($_COOKIE['user'])){
                echo '
            <a href="login" class="button">
            <i class="fas fa-user"></i>
            Sign in
            </a>  ';}
            else
                echo '
            <form method="post"><button class="button logout-button" type="submit" name="logout" value="logout">
            <i class="fas fa-sign-out-alt"></i>
            Logout
            </button>
            </form>
             ';
            ?>
        </li>
    </ul>

</div>

