<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/navigation.css">
    <link rel="stylesheet" type="text/css" href="public/css/header.css">
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/videos.css">
    <script src="https://kit.fontawesome.com/5e35f9208d.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="./public/js/search.js" defer></script>
    <script type="text/javascript" src="./public/js/statistics.js" defer></script>
    <script type="text/javascript" src="./public/js/nav.js" defer></script>
    <title>VIDEOS</title>
</head>

<body>
<div class="base-containter">
    <? include 'nav.php'; ?>
    <main id="main-containter">
        <? include 'header.php'; ?>
        <section class="videos" >
            <?php foreach($videos as $video): ?>
            <div id="<?= $video->getId() ?>">
                <img src="public/uploads/<?= $video->getImage() ?: "/test.png" ?>">
                <div>
                    <h2><?= $video->getTitle() ?></h2>
                    <h5 class="video-description"><?= $video->getDescription() ?></h5>
                    <div class="social-section">
                        <i class="fas fa-heart"> <?= $video->getLike() ?></i>
                        <i class="fas fa-minus-square"> <?= $video->getDislike() ?></i>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </section>
    </main>
</div>
</body>

<template id="video-template">
    <div id="">
        <img src="">
        <div>
            <h2>title</h2>
            <h5 class="video-description">description</h5>
            <div class="social-section" id="social">
                <i class="fas fa-heart"> 0</i>
                <i class="fas fa-minus-square"> 0</i>
            </div>
        </div>
    </div>
</template>