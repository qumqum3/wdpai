<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/navigation.css">
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/videos.css">
    <script src="https://kit.fontawesome.com/5e35f9208d.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="./public/js/nav.js" defer></script>
    <title>VIDEOS</title>
</head>


<body>
<div class="base-containter">
    <? include 'nav.php'; ?>
    <main id="main-containter" class="video-containter">
        <section class="video-form">
            <h1>Upload</h1>
            <form action="addVideo" method="POST" enctype="multipart/form-data">
                <?php
                if(isset($messages)){
                    foreach($messages as $message){
                        echo $message;
                    }
                }
                ?>
                <input name="title" type="text" placeholder="title">
                <textarea name="description" rows="5" type="text" placeholder="description"></textarea>

                <input type="file" name="file">
                <br>
                <button class="add-video" type="sumbit">send</button>
            </form>
        </section>
    </main>
</div>
</body>