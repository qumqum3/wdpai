<!DOCTYPE html>
<head>
    <link rel="stylesheet" type="text/css" href="public/css/navigation.css">
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/videos.css">
    <link rel="stylesheet" type="text/css" href="public/css/settings.css">
    <link rel="stylesheet" type="text/css" href="public/css/header.css">
    <script src="https://kit.fontawesome.com/5e35f9208d.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="./public/js/nav.js" defer></script>
    <title>SETTINGS</title>
</head>


<body>
<div class="base-containter">
    <? include 'nav.php'; ?>
    <main id="main-containter" class="video-containter">
        <? include 'basic-header.php'; ?>
        <section class="video-form">
            <div class="settings-containter">
            <h1>Settings</h1>
            <?php
            if($userType == "viewer"){
                echo '
            <form method="post" action="upgrade">
                <div class="card">
                    <div class="profile-changes">
                        <div> Click if you want to upgrade your account </div>
                    </div>
                    <button type="submit" name="upgrade" value="upgrade">Upgrade me!</button>
                </div>
            </form>  ';}
            else
                echo '
                <div> You have nothing to do here </div>
             ';
            ?>
            </div>
        </section>
    </main>
</div>
</body>