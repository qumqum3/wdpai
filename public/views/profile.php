<!DOCTYPE html>


<head>
    <link rel="stylesheet" type="text/css" href="public/css/navigation.css">
    <link rel="stylesheet" type="text/css" href="public/css/style.css">
    <link rel="stylesheet" type="text/css" href="public/css/videos.css">
    <link rel="stylesheet" type="text/css" href="public/css/profile.css">
    <link rel="stylesheet" type="text/css" href="public/css/header.css">
    <script src="https://kit.fontawesome.com/5e35f9208d.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="./public/js/nav.js" defer></script>
    <title>PROFILE</title>
</head>


<body>
<div class="base-containter">
    <? include 'nav.php'; ?>
    <main id="main-containter" class="video-containter">
        <? include 'basic-header.php'; ?>
        <section class="video-form">
            <h1 id="h1-profile">Profile</h1>

            <div class="card">
            <img src="public/profiles/<?=$user->getPhoto()?>" style="width:100%">
                <h2><?= $user->getName() ?> <?= $user->getSurname() ?></h2>
                <h2><?= $user->getEmail() ?></h2>
                <a href="#"><i class="fa fa-phone"></i> <?= $user->getPhone() ?></a>
                <br>
                <a href="#"><i class="fa fa-twitter"></i></a> &nbsp
                <a href="#"><i class="fa fa-linkedin"></i></a> &nbsp
                <a href="#"><i class="fa fa-facebook"></i></a>
            </div>

            <form method="post" action="change">
                <div class="card">
                    <div class="profile-changes">
                        <div> New name </div>
                            <input type="text" name="name">
                    </div>
                    <div class="profile-changes">
                        <div> New surname </div>

                        <input type="text" name="surname">

                    </div>
                    <div class="profile-changes">
                        <div> New phone </div>
                        <input type="text" name="phone">
                    </div>
                    <button type="submit" name="change_profile" value="change_profile">Save</button>
                </div>
            </form>
        </section>
    </main>
</div>

</body>