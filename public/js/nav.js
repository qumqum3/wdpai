function openNav() {
    document.getElementById("my-nav").style.width = "100%";

}

function closeNav() {
    document.getElementById("my-nav").style.width = "0";
}


function hideNav(){
    if(window.outerWidth < 768) {
        document.getElementById("my-nav").classList.remove("open-nav");
        document.getElementById("main-containter").style.marginLeft= "0";
    }
    else
    {
        document.getElementById("my-nav").classList.add("open-nav");
        document.getElementById("closebtn").style.display = "none";
    }
}

hideNav();