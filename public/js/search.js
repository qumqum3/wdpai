const search = document.querySelector('input[placeholder="Search video"]');
const videoContainer = document.querySelector(".videos");

search.addEventListener("keyup", function(event)
{
    if(event.key === "Enter")
    {
        event.preventDefault();
        const data = {search: this.value};

        fetch("/search", {
            method: "POST",
            headers: {
                "Content-Type": 'application/json'
            },
            body: JSON.stringify(data)
        }).then(function (response) {
            return response.json();
        }).then(function(videos){
            videoContainer.innerHTML = "";
            loadVideos(videos)
        });
    }
});

function loadVideos(videos) {

    videos.forEach(video => {
        createVideo(video);
    });
}

function createVideo(video) {
    const template = document.querySelector("#video-template");

    const clone = template.content.cloneNode(true);

    const div = clone.querySelector("div");
    div.id = video.id;

    const image = clone.querySelector("img");
    image.src = `/public/uploads/${video.video_name}`;

    const title = clone.querySelector("h2");
    title.innerHTML = video.title;
    const description = clone.querySelector("h5");
    description.innerHTML = video.description;
    const like = clone.querySelector(".fa-heart");
    like.innerHTML = ' ' + video.like;
    const dislike = clone.querySelector(".fa-minus-square");
    dislike.innerHTML = ' ' +  video.dislike;
    videoContainer.appendChild(clone);
}